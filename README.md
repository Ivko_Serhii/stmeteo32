### STMeteo32 ###
Simple meteostation based on STM32F407 Discovery kit.

- Temperature sensor: LM335Z (-24..100 C)
- Humidity sensor: DHT11
- Display : LCD1602


