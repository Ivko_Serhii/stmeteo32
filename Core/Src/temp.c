#include "stm32f4xx_hal.h"

#define EX_TEMP_MAX_TEMP	101
#define EX_TEMP_TEMP_RANGE	124
#define EX_TEMP_VOLT_RANGE	2.5

#define ADC_MAX_NUM		4096
#define ADC_REF_VOLT		3.0

extern ADC_HandleTypeDef hadc1;
static ADC_HandleTypeDef* hadc = &hadc1;

int ex_temp_read(float *temp)
{
	uint16_t adc_val;
	float voltage;
	float tmp;

	if (!temp) return -1;
	HAL_ADC_Start(hadc);
	HAL_ADC_PollForConversion(hadc,100);
	adc_val = HAL_ADC_GetValue(hadc);
	HAL_ADC_Stop(hadc);

	voltage = ADC_REF_VOLT / ADC_MAX_NUM * adc_val;
	// Can be up to -24 Celsius
	tmp = EX_TEMP_MAX_TEMP - (EX_TEMP_TEMP_RANGE / EX_TEMP_VOLT_RANGE * voltage);
	*temp = tmp;

	return 0;
}
