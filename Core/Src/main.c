/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdbool.h>
#include "usbd_cdc_if.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define RIGHT_BTN 1
#define OK_BTN 2
#define LEFT_BTN 3
#define UP_BTN 4
#define DOWN_BTN 5

#define TEMP_MARGIN  0 // deg C, ignore temperature changes less than TEMP_MARGIN
#define HUMID_MARGIN 1 // %, ignore humidity changes less than HUMID_MARGIN

#define UP_ARROW '\2'
#define DN_ARROW '\3'

#define STOP_STATE	0
#define LOG_STATE	1
#define STAT_STATE	2
#define CONF_STATE	3

#define abs(x)  ( ( (x) < 0) ? -(x) : (x) )
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim7;

/* USER CODE BEGIN PV */
bool do_refresh = false;
uint8_t btn = 0;

int temp_th = 30; 	// Temperature warning threshold
int humid_th = 75; 	// Humidity warning threshold

uint8_t usb_cdc_state = STOP_STATE;
uint16_t size;
char info_str[16];
char str_tx[160];
char str_rx[80];

Lcd_HandleTypeDef lcd;

BMP280_HandleTypedef bmp280;


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM6_Init(void);
static void MX_TIM7_Init(void);
static void MX_I2C1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void print_humid_min_max(uint8_t min, uint8_t max)
{
	    Lcd_cursor(&lcd, 0, 0);
	    Lcd_string(&lcd, "    Humidity    ");
	    Lcd_cursor(&lcd, 1, 0);
	    sprintf(info_str, "Lo:%2d%%   Hi:%2d%%", min, max);
	    Lcd_string(&lcd, info_str);

}

void print_temp_min_max(uint8_t min, uint8_t max)
{
	    Lcd_cursor(&lcd, 0, 0);
	    Lcd_string(&lcd, "  Temperature   ");
	    Lcd_cursor(&lcd, 1, 0);
	    sprintf(info_str, "Lo:%3dC Hi:%3dC", min, max);
	    Lcd_string(&lcd, info_str);

}


void parse_usb_cmd(char* cmd_str)
{
	uint8_t tmp;
	/* Parse console command */
	if (strncmp(cmd_str, "log", 3) == 0) {
		usb_cdc_state = LOG_STATE;
	} else if (strncmp(cmd_str, "stat", 4) == 0) {
		usb_cdc_state = STAT_STATE;
	} else if (strncmp(cmd_str, "stop", 4) == 0) {
		usb_cdc_state = STOP_STATE;
	} else if (strncmp(cmd_str, "thld", 4) == 0) {
		usb_cdc_state = CONF_STATE;
	} else if (strncmp(cmd_str, "temp_th", 7) == 0) {
		tmp = temp_th;
		if (sscanf(cmd_str, "temp_th %d", &temp_th) != 1){
			temp_th = tmp;
		}
	} else if (strncmp(cmd_str, "humid_th", 8) == 0) {
		tmp = humid_th;
		if (sscanf(cmd_str, "humid_th %d", &humid_th) != 1){
			humid_th = tmp;
		}
	}

}

void USB_CDC_Recv_Callback(uint8_t* Buf, uint32_t Len)
{
	static unsigned pos = 0;
	unsigned i = 0;
	uint8_t c;
	while(i < Len && (c=Buf[i]) != '\r'){
		str_rx[pos] = (char)c;
		pos++;
		i++;
	}
	if(c=='\r') {
		str_rx[pos] = '\0';
		/* Echo received command */
		sprintf(str_tx, "> %s\r\n", str_rx);
		CDC_Transmit_FS((unsigned char*)str_tx, strlen(str_tx));
		/* -------------------- */
		parse_usb_cmd(str_rx);
		pos = 0;
	}

}


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  MX_USB_DEVICE_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_Base_Start(&htim6);
  HAL_TIM_Base_Start_IT(&htim7);


  Lcd_PortType ports[] = {
          GPIOE, GPIOE, GPIOE, GPIOE };
  Lcd_PinType pins[] = { GPIO_PIN_12, GPIO_PIN_13, GPIO_PIN_14, GPIO_PIN_15 };

  lcd = Lcd_create(ports, pins, GPIOE, GPIO_PIN_7, GPIOE, GPIO_PIN_11, LCD_4_BIT_MODE);

  uint8_t deg_char[] = { 0x02, 0x05, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00};
  uint8_t up_char[]  = { 0x04, 0x0E, 0x1F, 0x04, 0x04, 0x04, 0x04, 0x04};
  uint8_t dn_char[]  = { 0x04, 0x04, 0x04, 0x04, 0x04, 0x1F, 0x0E, 0x04};

  Lcd_create_char(&lcd, 1, deg_char);
  Lcd_create_char(&lcd, 2, up_char);
  Lcd_create_char(&lcd, 3, dn_char);


  bmp280_init_default_params(&bmp280.params);
  bmp280.addr = BMP280_I2C_ADDRESS_0;
  bmp280.i2c = &hi2c1;

  while (!bmp280_init(&bmp280, &bmp280.params)) {
	size = sprintf(str_tx, "BMP280 initialization failed\n");
	CDC_Transmit_FS((unsigned char*)str_tx, size);
  }

  float bmp_temp, bmp_press, bmp_humid;

  float ex_temp = 0;

  int press_mm; // pressure in mm Hg
  uint8_t humid = 0, temp = 0;
  uint8_t temp_old, humid_old;
  uint8_t humid_max = 0, humid_min = 255;
  uint8_t temp_max = 0, temp_min = 255;

  char t_trend, h_trend;

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  /* Greeting */
    Lcd_cursor(&lcd, 0, 0);
    Lcd_string(&lcd, "STMeteo32");


  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    if(do_refresh) {

	    HAL_GPIO_WritePin(LD5_GPIO_Port, LD5_Pin, GPIO_PIN_RESET);
	    /* Try read hudity sensor */
	    if (DHT11_Read_Data(&temp, &humid))
		    HAL_GPIO_WritePin(LD5_GPIO_Port, LD5_Pin, GPIO_PIN_SET); //Error Led

	    /* Try read external temp sensor */
	    if (! ex_temp_read(&ex_temp))
		    temp = (int)ex_temp;  // OK
	    else
		    HAL_GPIO_WritePin(LD5_GPIO_Port, LD5_Pin, GPIO_PIN_SET); //Error Led

	    /* Read BMP280 sensor */
		while (!bmp280_read_float(&bmp280, &bmp_temp, &bmp_press, &bmp_humid)) {
			size = sprintf(str_tx,
					"BMP 280 Temperature/pressure reading failed\r\n");
			CDC_Transmit_FS((unsigned char*)str_tx, size);
			HAL_GPIO_WritePin(LD5_GPIO_Port, LD5_Pin, GPIO_PIN_SET);
		}



	    temp_max = (temp>=temp_max)? temp : temp_max;
	    temp_min = (temp<=temp_min)? temp : temp_min;
	    humid_max = (humid>=humid_max)? humid : humid_max;
	    humid_min = (humid<=humid_min)? humid : humid_min;

	    if(abs(temp - temp_old) > TEMP_MARGIN )
		  t_trend = (temp>temp_old)? UP_ARROW : DN_ARROW;
	    else
		  t_trend = '=';

	    if (abs(humid - humid_old) > HUMID_MARGIN)
		    h_trend = (humid>humid_old)? UP_ARROW : DN_ARROW;
	    else
		    h_trend = '=';

	    temp_old = temp;
	    humid_old = humid;

	    press_mm = (int)(bmp_press * 0.0075);
	    sprintf(info_str, " STMeteo P=%dmm", press_mm);
	    Lcd_cursor(&lcd, 0, 0);
	    Lcd_string(&lcd, info_str);

	    if(temp>=temp_th || humid>=humid_th) {
		    Lcd_cursor(&lcd, 0, 0);
		    Lcd_string(&lcd, "\xed"); //Display bell
	    }

	    sprintf(info_str, "T%c%3d\1C Rh%c%3d%%", t_trend, temp, h_trend, humid);
	    Lcd_cursor(&lcd, 1, 0);
	    Lcd_string(&lcd, info_str);

	    /* USB Virtual Serial Port Logging */
	   if (usb_cdc_state == LOG_STATE) {
		    sprintf(str_tx, "Temp =%3d\xE2\x84\x83 Rh=%3d%% P=%dmm\r\n", temp, humid, press_mm);
		    CDC_Transmit_FS((unsigned char*)str_tx, strlen(str_tx));
	  }

	    /* ------------------------------------ */

	    HAL_GPIO_WritePin( LD4_GPIO_Port, LD4_Pin, GPIO_PIN_RESET );
	    do_refresh = false;
    }

    switch(usb_cdc_state){
    case STAT_STATE:
	    sprintf(str_tx,
			    "STMeteo32 Statistics\r\nTemp =%3d\xE2\x84\x83 (min:%3d max:%3d)\r\nRhum =%3d%% (min:%3d max:%3d)\r\nP = %dmm\r\n",
			    temp, temp_min, temp_max, humid, humid_min, humid_max, press_mm);
	    CDC_Transmit_FS((unsigned char*)str_tx, strlen(str_tx));

	    usb_cdc_state = STOP_STATE;
	    break;
    case CONF_STATE:
	    sprintf(str_tx, "Thresholds\r\nTemp:%d C\r\nRhum:%d%%\r\n", temp_th, humid_th);
	    CDC_Transmit_FS((unsigned char*)str_tx, strlen(str_tx));

	    usb_cdc_state = STOP_STATE;
	    break;
    default:
	    break;

    }

    switch(btn){
    case OK_BTN:
	    break;
    case LEFT_BTN:
	    print_humid_min_max(humid_min, humid_max);
	    break;
    case RIGHT_BTN:
	    print_temp_min_max(temp_min, temp_max);
	    break;
    case UP_BTN:
	    break;
    case DOWN_BTN:
	    break;
    default:
	    break;

    }
    btn = 0;

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV8;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_9;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_15CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 42-1;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 0xffff-1;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * @brief TIM7 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM7_Init(void)
{

  /* USER CODE BEGIN TIM7_Init 0 */

  /* USER CODE END TIM7_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM7_Init 1 */

  /* USER CODE END TIM7_Init 1 */
  htim7.Instance = TIM7;
  htim7.Init.Prescaler = 42000-1;
  htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim7.Init.Period = 3000-1;
  htim7.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM7_Init 2 */

  /* USER CODE END TIM7_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(OTG_FS_PowerSwitchOn_GPIO_Port, OTG_FS_PowerSwitchOn_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, DISP_RS_Pin|DISP_RW_Pin|DISP_ENA_Pin|DISP_DB4_Pin 
                          |DISP_DB5_Pin|DISP_DB6_Pin|DISP_DB7_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, LD4_Pin|GPIO_PIN_13|LD5_Pin|LD6_Pin 
                          |Audio_RST_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : OTG_FS_PowerSwitchOn_Pin */
  GPIO_InitStruct.Pin = OTG_FS_PowerSwitchOn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(OTG_FS_PowerSwitchOn_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PB2 */
  GPIO_InitStruct.Pin = GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : DISP_RS_Pin DISP_RW_Pin DISP_ENA_Pin DISP_DB4_Pin 
                           DISP_DB5_Pin DISP_DB6_Pin DISP_DB7_Pin */
  GPIO_InitStruct.Pin = DISP_RS_Pin|DISP_RW_Pin|DISP_ENA_Pin|DISP_DB4_Pin 
                          |DISP_DB5_Pin|DISP_DB6_Pin|DISP_DB7_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : DHT11_IO_Pin OTG_FS_OverCurrent_Pin */
  GPIO_InitStruct.Pin = DHT11_IO_Pin|OTG_FS_OverCurrent_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : LD4_Pin PD13 LD5_Pin LD6_Pin 
                           Audio_RST_Pin */
  GPIO_InitStruct.Pin = LD4_Pin|GPIO_PIN_13|LD5_Pin|LD6_Pin 
                          |Audio_RST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : LT_BTN_EXTI_Pin RT_BTN_EXTI_Pin */
  GPIO_InitStruct.Pin = LT_BTN_EXTI_Pin|RT_BTN_EXTI_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : OK_BTN_EXTI_Pin */
  GPIO_InitStruct.Pin = OK_BTN_EXTI_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(OK_BTN_EXTI_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : MEMS_INT2_Pin */
  GPIO_InitStruct.Pin = MEMS_INT2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(MEMS_INT2_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 2, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 2, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  if(htim->Instance == TIM7) {
	    //HAL_TIM_Base_Stop_IT(htim);
	  do_refresh = true;
	  HAL_GPIO_WritePin( LD4_GPIO_Port, LD4_Pin, GPIO_PIN_SET );
  }
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{

	switch (GPIO_Pin){
	case OK_BTN_EXTI_Pin:
		btn = OK_BTN;
		break;
	case RT_BTN_EXTI_Pin:
		btn = RIGHT_BTN;
		break;
	case LT_BTN_EXTI_Pin:
		btn = LEFT_BTN;
		break;
	default:
		break;

	}

}



/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
